package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {
    private int[] digitos;
    Map <Integer,String>parseoNumeros = new HashMap<Integer,String>();
    public static void main(String[] args) {
        MontoEscrito montoEscrito = new MontoEscrito();
        montoEscrito.getMontoEscrito(0);
    }
    public String getMontoEscrito(int valor){
        String resultado = "";
        String valorString = String.valueOf(valor);
        int[] digitos = new int[valorString.length()];
        int j = digitos.length - 1;
        int numero = valor;

        while (numero > 0) {
            digitos[j] = numero % 10;
            numero = numero / 10;
            j--;
        }

        int cont = 0;

        for (int i = 0; i < digitos.length; i++) {
            cont ++;

        }

        crearParseNumeros();

        if(parseoNumeros.containsKey(valor)){
            resultado = parseoNumeros.get(valor);
        }else{
            if (cont > 2 && valor != 500) {
                for (int i = 0; i < digitos.length; i++) {
                    String decenaString = String.valueOf(digitos[1]) + "0";
                    String decena = parseoNumeros.get(Integer.valueOf(decenaString));
                    String unidad = parseoNumeros.get(digitos[2]);
                    int primerDigito = inicializaDigitos(valor);
                    if(digitos[0] ==1 && digitos[1]==0){
                        resultado = "ciento " + " " +  unidad;
                    }else{
                        if(digitos[0] ==1 && digitos[1] !=0){
                            resultado =  "ciento " + decena + " y " + unidad;
                        }else{
                            resultado = parseoNumeros.get(primerDigito) + "cientos " + decena + " y " + unidad;
                        }
                    }
                    if(digitos[0] == 5){
                        resultado =   "quinientos " + decena + " y " + unidad;
                    }

                }
                if (cont > 3) {
                    for (int i = 0; i < digitos.length; i++) {
                        int primerDigito = inicializaDigitos(valor);
                        String centena = parseoNumeros.get(digitos[1]);
                        String decenaString = String.valueOf(digitos[2]) + "0";
                        String decena = parseoNumeros.get(Integer.valueOf(decenaString));
                        String unidad = parseoNumeros.get(digitos[3]);
                        if (digitos[0] == 1 && digitos[1] != 0 && digitos[2] != 0 && digitos[3] != 0) {
                            resultado =  " mil " + centena
                                    + "cientos " + decena + " y " + unidad;
                        } else
                        if (digitos[0] == 1 && digitos[1] == 0 && digitos[2] == 0 && digitos[3] != 0) {
                            resultado = " mil " + unidad;
                        } else {
                            if (digitos[0] == 1 && digitos[1] == 0 && digitos[2] != 0 && digitos[3] != 0) {
                                resultado = " mil " + decena + " y " + unidad;
                            } else {
                                if (digitos[0] == 1 && digitos[1] != 0 && digitos[2] == 0 && digitos[3] != 0) {
                                    resultado = " mil " + centena
                                            + "cientos " + unidad;
                                }else{
                                    if (digitos[0] != 0 && digitos[1] != 0 && digitos[2] != 0 && digitos[3] == 0) {
                                        resultado = parseoNumeros.get(primerDigito) + " mil " + centena + "cientos " + decena;
                                    }else{
                                        resultado = parseoNumeros.get(primerDigito) + " mil " + centena
                                                + "cientos " + decena + " y " + unidad;
                                    }
                                }
                            }
                        }



                        if (centena.equals("uno")) {
                            resultado = resultado.replace(centena, "");
                            resultado = resultado.replace("cientos", "ciento");
                        }
                    }
                }

                if (cont > 4) {
                    for (int i = 0; i < digitos.length; i++) {
                        String primerDigitoString = String.valueOf(inicializaDigitos(valor)) + "0";
                        String primerDigito = parseoNumeros.get(Integer.valueOf(primerDigitoString));
                        String unidadMil = parseoNumeros.get(digitos[1]);
                        String centena = parseoNumeros.get(digitos[2]);
                        String decenaString = String.valueOf(digitos[3]) + "0";
                        String decena = parseoNumeros.get(Integer.valueOf(decenaString));
                        String unidad = parseoNumeros.get(digitos[4]);

                        resultado = primerDigito + " y " + unidadMil + " mil " + centena + "cientos "
                                + decena + " y " + unidad;
                    }
                }

                if (cont > 5) {
                    for (int i = 0; i < digitos.length; i++) {
                        int primerDigito = inicializaDigitos(valor);
                        String decenaMilString = String.valueOf(digitos[1]) + "0";
                        String decenaMil = parseoNumeros.get(Integer.valueOf(decenaMilString));
                        String unidadMil = parseoNumeros.get(digitos[2]);
                        String centena = parseoNumeros.get(digitos[3]);
                        String decenaString = String.valueOf(digitos[4]) + "0";
                        String decena = parseoNumeros.get(Integer.valueOf(decenaString));
                        String unidad = parseoNumeros.get(digitos[5]);
                        resultado = parseoNumeros.get(primerDigito) + "cientos " + decenaMil + " y " + unidadMil
                                + " mil " + centena + "cientos " + decena + " y " + unidad;

                    }
                }

                if (cont > 6) {
                    for (int i = 0; i < digitos.length; i++) {
                        int primerDigito = inicializaDigitos(valor);
                        String centenaMil = parseoNumeros.get(digitos[1]);
                        String decenaMilString = String.valueOf(digitos[2]) + "0";
                        String decenaMil = parseoNumeros.get(Integer.valueOf(decenaMilString));
                        String unidadMil = parseoNumeros.get(digitos[3]);
                        String centena = parseoNumeros.get(digitos[4]);
                        String decenaString = String.valueOf(digitos[5]) + "0";
                        String decena = parseoNumeros.get(Integer.valueOf(decenaString));
                        String unidad = parseoNumeros.get(digitos[6]);
                        resultado = parseoNumeros.get(primerDigito) + " millones " + centenaMil + "cientos "
                                + decenaMil + " y " + unidadMil + " mil " + centena + "cientos " + decena + " y "
                                + unidad;

                        if (unidadMil.equals("cero")) {
                            resultado = resultado.replace(unidadMil, "");
                            resultado = resultado.replaceFirst(" y ", "");
                        }
                    }
                }

            }
        }




        return resultado;
    }

    private int inicializaDigitos(int numero) {

        String numeroToString = Integer.toString(numero);

        digitos = new int[numeroToString.length()];
        int j = 0;
        for (int i = numeroToString.length() - 1; i > 0; i--) {
            digitos[j++] = Integer.parseInt(numeroToString.substring(i, i + 1));
        }
        digitos[j] = Integer.parseInt(numeroToString.substring(0, 1));

        return digitos[j];

    }

    private void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");

    }


}
